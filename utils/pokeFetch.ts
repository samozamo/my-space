const baseURL = "https://pokeapi.co/api/v2/";

export default async function (url: string) {
  const res = await fetch(`${baseURL}${url}`);
  const resJSON = await res.json();
  return resJSON;
}
