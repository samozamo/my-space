type NameUrl = {
  name: string;
  url: string;
};

type PokemonSpecies = NameUrl;
type Language = NameUrl;
type Region = NameUrl;
type VersionGroup = NameUrl;
type PokedexResult = NameUrl;

type PokedexRes = {
  count: string;
  next: string;
  results: PokedexResult[];
};

type PokemonEntry = {
  entry_number: number;
  pokemon_species: PokemonSpecies;
};

type Description = {
  description: string;
  language: Language;
};

type Name = {
  name: string;
  language: Language;
};

type PokedexEntry = {
  id: number;
  name: string;
  descriptions: Description[];
  names: Name[];
  pokemon_entries: PokemonEntry[];
  region: Region;
  version_groups: VersionGroup[];
};
