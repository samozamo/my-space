// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  app: {
    baseURL: "/my-space/",
  },
  nitro: {
    output: {
      publicDir: "public",
    },
  },
  modules: [
    [
      "@nuxtjs/google-fonts",
      {
        families: {
          Rubik: true,
        },
        download: true,
      },
    ],
    "@nuxt/image",
  ],
  dir: {
    public: "static",
  },
});
